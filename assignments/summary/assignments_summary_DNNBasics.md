**Neural Networks**

A neural network is a network of connected neurons which can be made to learn, recognize patterns and make decision similar to how a human brain functions. A neural network need not be explicitly programmed, it can learn patterns and behaviours all by itself.

![Neural Network](NeuralNW.PNG)

***Deep neural network***

It is  a type of neural networks with usually more than 2 layers. It is a more sophisticated modeling process which can compute more complex data.

***Working***

![Working of DNN](Sigmoid.PNG)

- A neural network consists of multiple layers. Each layer performs a different task. The first later, i.e. input layer, picks up input signal and passes it to next layer. 

- The following layer, hidden layer, performs calculations and feature extraction. There can be more than one hidden layer depending on the amount of detail to be extracted. Hidden layers can perform linear transformations on inputs using weigths and bias. Various Activation functions are also employed post this step.

- Lastly there is an output layer, also known as fully connected layer which dislpays the results.

***Important Terms***
 
 *Training*: Here intially the weigths are assigned random values, which the neural network then refines after each passing iteration as it starts to learn the features. This is known as Training the neural network.

 *Gradient Descent*: Gradient of the value gives the direction of steepest ascent.
 Gradient descent is an efficient optimization algorithm that attempts to find a local or global minima of a function.
 Gradient descent enables a model to learn the gradient or direction that the model should take in order to reduce errors.

 *Stochastic gradient Descent*: Stochastic gradient descent (abbr SGD) is an iterative method for optimizing an objective function with suitable smoothness properties.

 *Error*: This determines how well a network performing during the training. Each iteration in the neural network tries to minimize the error.associated with each step.

 *Cost Function*: Cost Function quantifies the error between predicted values and expected values and presents it in the form of a single real number.

 *Back Propagation*: To minimise the error, you propagate backwards by finding the derivative of error with respect to each weight and then subtracting this value from the weight value. This is known as back propagation.

 *Sigmoid*: Sigmoid has a smooth gradient and outputs values between zero and one.






