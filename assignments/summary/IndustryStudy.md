**Product Name**

Visual Leak Defect Detection

**Product Link**

[Link](https://landing.ai/defect-detection/)

**Product Short Description**

Able to accurately determine all shapes and sizes of leaks as well as slow or fast leaks with only 1-10 training defect images.

**Product is combination of features**

1. Object detection

**Product is provided by which company?**

Landing.ai

---------------------------------------------------------------------------------------------------------------------------------------------

**Product Name**

Shape Scan System

**Product Link**

[Link](https://www.argutec.eu/products)

**Product Short Description**

Using an array of lasers it provides a contactless monitering system  allowing shape and dimension measurement of a product

**Product is combination of features**

1. Object detection
2. ID Detection

**Product is provided by which company?**

Argutec

---------------------------------------------------------------------------------------------------------------------------------------------

**Product Name**

Baggage and Parcel inspection

**Product Link**

[Link](https://www.rapiscansystems.com/en/products/category/baggage-and-parcel-inspection)

**Product Short Description**

A versatile system able to adapt to different screening scenarios providing the best performance and functionality while being  effective, efficient, and being able to meet the toughest regulatory requirements.

**Product is combination of features**

1. Object detection
2. ID Detection

**Product is provided by which company?**

Rapiscan Systems




