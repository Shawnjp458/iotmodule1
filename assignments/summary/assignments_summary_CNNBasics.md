**Convolutional Neural Networks**

***Definition***

A Convolutional neural network(CNN) is a type of neural network which has found widespread use in the fields image recognition, classification  and object detection.

![Convolutional Neural Network](Conv.PNG)

***Architecture***

The architecture of a CNN consists of the following:

- *Convolutional layer*
    
    Convolutional layers convolve the input and pass its result to the next layer. It consists of various neurons that process data and pass it on to further neurons.
    ![Layer1](Layer1.PNG)
- *Pooling Layer* 
    
    Pooling layers reduce the dimensions of the data by combining the outputs of multiple neurons at one layer into a single neuron in the next layer. Depending on the use case *max-pooling* or *average-pooling* is applied.
    
     ![Layer2](Layer2.PNG)
- *Fully Connected Layer*
    
    Fully connected layers connect every neuron in one layer to every neuron in another layer. THe output matrix of previous layers passes through the fully-connected layer to create a model for classifying images.
     
     ![Layer3](Layer3.PNG)

***How it works***

- The first layer in a CNN network is known as the *Convolutional layer*. Here we begin by implementing a filter which can  capture the important features of the images we are trying to process.
- Next we have the *Activation Layer* which applies the activation function(often ReLu) to increase the non-lineraity in the CNN
- The next layer to follow is the *Pooling Layer* which downsamples the features with the help of hyperparameters.
- Finally we have the *Fully connected Layer* in which the process of Flattening  takes place. Her eall the features are combined to create a model.

Once the model is generated we use an activation function called *softmax* which can be used to classify the output.

***Keywords***

Kernel: They are the filters applied during convolution.

Stride: It is the number of pixels by which the filter shifts over the input matrix.

Flattening: Flattening layer is used to bring all feature maps matrices into a single vector.

ReLU(Rectified Linear Unit): Activation function that introduces non-linearity(to learn non-negative liner values).

Padding: Provides control of the output spatial size. IT is of 2 types: *Zero padding* and *Valid padding*

